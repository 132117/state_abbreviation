##State Name Abbreviation

1) *Introduction*

    This script takes orders from bigcommerce and looks at the id of the last order it checked in the seed_file. It requests all orders with numerical id higher than the seed number and then alters the State form in their billing and shipping address sections.

1) *Usage*

    1) To run the script put the id of the order you would like to start with in the seed file.
    1) Run `python abbreviate.py --clientId=<BC Client Id> --APIToken=<BC API Token --StoreHash=<BC Store Hash>`
    1) To trigger the execution send post requests to the port it is running on.
    
    ***Webhooks can be used to send requests to the server. Since hostgator with Cpanel does not allow running custom apps on ports, alternatively a cron job that sends an internal post request can be used.***
