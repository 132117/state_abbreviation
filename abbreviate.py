import requests
import json
from math import ceil
from datetime import datetime
import argparse
import pandas as pd
import time
from flask import Flask, request

parser = argparse.ArgumentParser(description='Update Clearance Category on Products')
parser.add_argument('--clientId', help='Client ID for headers')
parser.add_argument('--APIToken', help='Access token')#setup arguments for the script
parser.add_argument('--StoreHash', help='Store hash')#supply the hash to reach the store
parser.add_argument('--port', help='Store hash')
args = parser.parse_args()

def get(url,params=None,headers=None):
    response_code=None
    while response_code!=200:
        try:
            r = requests.get(url,params=params,headers=headers)
            response_code=r.status_code
        except:
            response_code='No response'
        if response_code!=200:
            time.sleep(0.01)
    return r

def put(url,data=None,headers=None):
    response_code=None
    while response_code!=200 and response_code!=204:
        try:
            r = requests.put(url,data=data,headers=headers)
            response_code=r.status_code
        except:
            response_code='No response'
        if response_code!=200 and response_code!=204:
            time.sleep(0.01)
    return r

def post(url,data=None,headers=None):
    response_code=None
    while response_code!=200 and response_code!=204:
        try:
            r = requests.post(url,data=data,headers=headers)
            response_code=r.status_code
        except:
            response_code='No response'
        if response_code!=200 and response_code!=204:
            time.sleep(0.01)
    return r

def delete(url,headers=None):
    response_code=None
    while response_code!=200 and response_code!=204:
        try:
            r = requests.delete(url,headers=headers)
            response_code=r.status_code
        except:
            response_code='No response'
        if response_code!=200 and response_code!=204:
            time.sleep(0.01)
    return r



app = Flask(__name__)

@app.route('/worker_trigger', methods=['POST'])
def result():
    print('working on new orders')
    #request_data=json.loads(request.data)
    seed_value=int(open('seed_value','r').read())
    #args = parser.parse_args()
    headers={"Accept": "application/json","Content-Type": "application/json","X-Auth-Client": args.clientId,"X-Auth-Token": args.APIToken}
    request_params={'min_id':seed_value}
    print('getting orders')
    r=get('https://api.bigcommerce.com/stores/%s/v2/orders'%args.StoreHash,
        params=request_params,
        headers=headers)
    print('loading jsons')
    orders_to_check=json.loads(r.content)
    print(orders_to_check[0])
    print(len(orders_to_check))

    states=pd.read_csv('states.csv')
    states['Key']=states['State']
    states=states.set_index('Key')
    r = get(orders_to_check[0]['shipping_addresses']['url'],headers=headers)
    addresses=json.loads(r.content)
    #orders_to_check=[x for x in orders_to_check if x['payment_method']=='Test Payment Gateway']
    print(len(orders_to_check))
    while len(orders_to_check)!=0:
        print(orders_to_check[0]['id'])
        order_id=orders_to_check[0]['id']
        print(order_id)
        r = get(orders_to_check[0]['shipping_addresses']['url'],headers=headers)
        addresses=json.loads(r.content)
        for x in range(len(addresses)):
            if len(addresses[x]['state'])>2:
                print('Prooooooooooooooooooooooooooooooblem detected')
                addresses[x]['state']=states.loc[addresses[x]['state'],'Abbreviation']
        if len(orders_to_check[0]['billing_address']['state'])>2:
            print('changing_billing_address')
            orders_to_check[0]['billing_address']['state']=states.loc[orders_to_check[0]['billing_address']['state'],'Abbreviation']
        orders_to_check[0]['shipping_addresses']=[{attr:shipping_addr[attr] for attr in shipping_addr.keys() if attr in ['id','order_id',"first_name","last_name","company","street_1","street_2","city","state","zip","country","country_iso2"]} for shipping_addr in addresses]
        print('This many addresses now',len(addresses))
        print(orders_to_check[0]['billing_address'])
        print(orders_to_check[0]['shipping_addresses'])
        r=requests.put('https://api.bigcommerce.com/stores/%s/v2/orders/'%args.StoreHash+str(order_id),
            data=json.dumps({'billing_address':orders_to_check[0]['billing_address'],'shipping_addresses':orders_to_check[0]['shipping_addresses']}),
            headers=headers)
        print(order_id,'State abbreviated')
        orders_to_check=orders_to_check[1:]
        seed_value=order_id
    progress_output=open('seed_value','w')
    progress_output.write(str(seed_value))
    progress_output.close()
    return "update_complete"


app.run(host='0.0.0.0',port=args.port)
